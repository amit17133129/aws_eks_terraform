*Launch an EKS cluster on AWS using Terraform*


- What is **EKS Cluster**:
- The Amazon EKS control plane consists of control plane nodes that run the Kubernetes software, such as etcd and the Kubernetes API    server. The control plane runs in an account managed by AWS, and the Kubernetes API is exposed via the Amazon EKS endpoint associated  with your cluster. Each Amazon EKS cluster control plane is single-tenant and unique, and runs on its own set of Amazon EC2 instances.

- So i created a user on aws and given respective permission so that it could have an access of ec2 instance.
- I collected access_key and secret_key of that user.
- using aws cli i created a profile using below command.
  *aws configure --profile profile_name*
  - you have to *provide access_key*, *secret_key* and *region* to this profile.
- the create a provider.tf file in a directory and then use aws provider so that we can provison the resources on aws.
- After that we have to create the **vpc.tf** and subnets inside that respective file.
- For enabling the user to access the instances we have to create security groups (firewall)
- In **Security_Groups.tf** you will find the code for creating security groups in aws.
- Then we have to create internet gateway so our vpc will be connected to public world. You will find code for creating internet gateway in **IG.tf** file.
- To route the path of internet gateway you have to create routing table. In **rout_table.tf** you will find code for creating routing table.
- you will find all the variable in the respective **variable.tf** file.
- We have to create some role before creating eks cluster. To create the role you will find code in iam_role.tf.
- Now we can create eks cluster from **eks.tf** file. There i have mentioned how many nodes you wanted. So my desired request is 2 and if the traffic is more so it will scale to *maximum to 3* nodes(instances) where as if the traafic is less so it will resccale to *minimum 1 node*.
```
    desired_size = 2
    max_size     = 3
    min_size     = 1
```

- Now we have to attach the iam role to this eks cluster. So you will find the respective code in **iam_role_attachment_eks.tf file**.
- you can use `terraform init` to initialize the terraform code and `terraform plan` for dry run. If all works fine then you have to run `terraform apply` to launch the resources on aws.
